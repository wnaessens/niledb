﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FsCheck;
using FsCheck.Xunit;
using Xunit.Abstractions;

namespace NileDB.Modules.WriteAheadLog; 

public class WriterTests : IDisposable {
    readonly ITestOutputHelper _output;

    int _writeCount;

    public WriterTests(ITestOutputHelper output) {
        _output = output ?? throw new ArgumentNullException(nameof(output));
        _writeCount = 0;
    }

    [Property]
    public void WritesRecordForEachPayload() {
        async Task<bool> CheckAsync(byte[][] payloads) {
            var stream = new MemoryStream();
            await using var writer = await Writer.CreateAsync(new WriterConfiguration(() => Task.FromResult<Stream>(stream), 1L));

            foreach (var payload in payloads) {
                await writer.WriteAsync(payload);
                _writeCount++;
            }
            
            return stream.Length == payloads.Sum(p => p.Length + 28);
        }
        
        Prop.ForAll<byte[][]>(payloads => {
            var t = CheckAsync(payloads);
            t.Wait();
            return t.Result;
        }).QuickCheck();

    }

    public void Dispose() {
        _output.WriteLine($"Performed {_writeCount:#,###} writes.");
    }
}