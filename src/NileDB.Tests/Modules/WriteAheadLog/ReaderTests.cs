﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using FsCheck;
using FsCheck.Xunit;
using Xunit;
using Xunit.Abstractions;

namespace NileDB.Modules.WriteAheadLog; 

public class ReaderTests : IDisposable {
    readonly ITestOutputHelper _output;

    int _writeCount;
    int _readCount;

    public ReaderTests(ITestOutputHelper output) {
        _output = output ?? throw new ArgumentNullException(nameof(output));
        _writeCount = 0;
        _readCount = 0;
    }

    [Property]
    public void WrittenRecordsCanBeReadBack() {
        async Task<bool> CheckAsync(byte[][] payloads) {
            var stream = new MemoryStream();
            await using var writer = await Writer.CreateAsync(new WriterConfiguration(() => Task.FromResult<Stream>(stream), 1L));

            foreach (var payload in payloads) {
                await writer.WriteAsync(payload);
                _writeCount++;
            }
            
            var readPayloads = new Reader(stream).Read().ToArray();
            _readCount += readPayloads.Length;
            
            return readPayloads == payloads && _writeCount == _readCount;
        }
        
        Prop.ForAll<byte[][]>(payloads => {
            var t = CheckAsync(payloads);
            t.Wait();
            return t.Result;
        }).QuickCheck();
    }

    public static IEnumerable<object> FaultyRecords() {
        yield return new object[] {
            // not enough bytes for the LEN field
            new byte[] { 0, 0, 1 }
        };

        yield return new object[] {
            // no more bytes after the LEN field
            new byte[] {0, 0, 0, 1 }
        };
        
        yield return new object[] {
            // only LEN and LSN fields specified
            new byte[] {
                /* LEN 1 */ 1, 0, 0, 0,
                /* LSN 1 */ 1, 0, 0, 0, 0, 0, 0, 0
            }
        };
        
        yield return new object[] {
            // only LEN, LSN and half a TIMESTAMP field
            new byte[] {
                /* LEN 1 */ 1, 0, 0, 0,
                /* LSN 1 */ 1, 0, 0, 0, 0, 0, 0, 0,
                /* TIMESTAMP */ 0, 0, 0, 0, 0
            }
        };
        
        yield return new object[] {
            new byte[] {
                /* LEN 1 */ 1, 0, 0, 0,
                /* LSN 1 */ 1, 0, 0, 0, 0, 0, 0, 0,
                /* TIMESTAMP */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                /* no payload, despite specifying LEN 1 */
                /* CRC */ 131, 118, 217, 169
            }
        };

        yield return new object[] {
            new byte[] {
                /* LEN 1 */ 1, 0, 0, 0,
                /* LSN 1 */ 1, 0, 0, 0, 0, 0, 0, 0,
                /* TIMESTAMP */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                /* PAYLOAD */ 1,
                /* wrong CRC (last byte should be 169) */ 131, 118, 217, 168
            }
        };

        yield return new object[] {
            new byte[] {
                // complete and correct record:
                /* LEN 1 */ 1, 0, 0, 0,
                /* LSN 1 */ 1, 0, 0, 0, 0, 0, 0, 0,
                /* TIMESTAMP */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                /* PAYLOAD */ 1,
                /* CRC */ 131, 118, 217, 169,

                /* padding makes for a rubbish stream tail: */
                0, 0, 0, 0
            }
        };
    }

    [Theory]
    [MemberData(nameof(FaultyRecords))]
    public void GivenFaultyRecord_ThenThrows(byte[] content) {
        var stream = new MemoryStream();
        stream.Write(content);

        var act = () => new Reader(stream).Read().ToArray();

        act.Should().Throw<WriteAheadLogReadException>();
    }

    public void Dispose() {
        _output.WriteLine($"Performed {_writeCount:#,###} writes and {_readCount:#,###} reads.");
    }
}