﻿namespace NileDB;

/// <summary>
/// Holds either one of two possible values, whereby one value is considered the happy case while the other is an error case.
/// </summary>
/// <typeparam name="T">The type of the happy case.</typeparam>
/// <typeparam name="TError">The type of the error case.</typeparam>
internal abstract class Result<T, TError> {
    /// <summary>
    /// Applies the given function to produce a new <see cref="Result{T,TError}"/>.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new <see cref="Result{T,TError}"/>.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public abstract Result<TU, TError> Bind<TU>(Func<T, Result<TU, TError>> f);

    /// <summary>
    /// Applies the given function to produce a new <see cref="Ok{T,TError}"/> case.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new value.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public abstract Result<TU, TError> Bind<TU>(Func<T, TU> f);

    /// <summary>
    /// Applies one of two given functions to produce a <typeparamref name="TU"/>.
    /// </summary>
    /// <typeparam name="TU">The type of the returned value.</typeparam>
    /// <param name="ok">Function to apply in case of an <see cref="Ok{T,TError}"/> result.</param>
    /// <param name="error">Function to apply in case of an <see cref="Error{T,TError}"/> result.</param>
    /// <returns>A <typeparamref name="TU"/>.</returns>
    public abstract TU Match<TU>(Func<T, TU> ok, Func<TError, TU> error);
}

/// <summary>
/// Holds the value of the happy case of a <see cref="Result{T,TError}"/>.
/// </summary>
internal class Ok<T, TError> : Result<T, TError> {
    readonly T _value;

    /// <summary>
    /// Initializes a new <see cref="Ok{T,TError}"/> result.
    /// </summary>
    /// <param name="value">The value of the result.</param>
    public Ok(T value) => _value = value;

    /// <summary>
    /// Applies the given function to produce a new <see cref="Result{T,TError}"/>.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new <see cref="Result{T,TError}"/>.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public override Result<TU, TError> Bind<TU>(Func<T, Result<TU, TError>> f) => f(_value);

    /// <summary>
    /// Applies the given function to produce a new <see cref="Result{T,TError}"/>.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new <see cref="Result{T,TError}"/>.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public override Result<TU, TError> Bind<TU>(Func<T, TU> f) => new Ok<TU, TError>(f(_value));

    /// <summary>
    /// Applies one of two given functions to produce a <typeparamref name="TU"/>.
    /// </summary>
    /// <typeparam name="TU">The type of the returned value.</typeparam>
    /// <param name="ok">Function to apply in case of an <see cref="Ok{T,TError}"/> result.</param>
    /// <param name="error">Function to apply in case of an <see cref="Error{T,TError}"/> result.</param>
    /// <returns>A <typeparamref name="TU"/>.</returns>
    public override TU Match<TU>(Func<T, TU> ok, Func<TError, TU> error) => ok(_value);
}

/// <summary>
/// Holds the value of the error case of a <see cref="Result{T,TError}"/>.
/// </summary>
internal class Error<T, TError> : Result<T, TError> {
    readonly TError _error;

    /// <summary>
    /// Initializes a new <see cref="Error{T,TError}"/> result.
    /// </summary>
    /// <param name="error">The value of the error.</param>
    public Error(TError error) => _error = error;

    /// <summary>
    /// Applies the given function to produce a new <see cref="Result{T,TError}"/>.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new <see cref="Result{T,TError}"/>.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public override Result<TU, TError> Bind<TU>(Func<T, Result<TU, TError>> f) => new Error<TU, TError>(_error);

    /// <summary>
    /// Applies the given function to produce a new <see cref="Ok{T,TError}"/> case.
    /// </summary>
    /// <typeparam name="TU">The type for the <see cref="Ok{T,TError}"/> case of the new <see cref="Result{T,TError}"/>.</typeparam>
    /// <param name="f">The function that produces the new value.</param>
    /// <returns>A <see cref="Result{TU,TError}"/></returns>
    public override Result<TU, TError> Bind<TU>(Func<T, TU> f) => new Error<TU, TError>(_error);

    /// <summary>
    /// Applies one of two given functions to produce a <typeparamref name="TU"/>.
    /// </summary>
    /// <typeparam name="TU">The type of the returned value.</typeparam>
    /// <param name="ok">Function to apply in case of an <see cref="Ok{T,TError}"/> result.</param>
    /// <param name="error">Function to apply in case of an <see cref="Error{T,TError}"/> result.</param>
    /// <returns>A <typeparamref name="TU"/>.</returns>
    public override TU Match<TU>(Func<T, TU> ok, Func<TError, TU> error) => error(_error);
}

internal static class Result {
    /// <summary>
    /// Returns a <see cref="Result{T,TError}"/>, based on the value of the given <paramref name="condition"/>.
    /// </summary>
    /// <typeparam name="T">The type of the value in the <see cref="Ok{T,TError}"/> case.</typeparam>
    /// <typeparam name="TError">The type of the error in the <see cref="Error{T,TError}"/> case.</typeparam>
    /// <param name="condition">WHen <c>true</c>, the <see cref="Ok{T,TError}"/> case is returned, otherwise the <see cref="Error{T,TError}"/> case is returned.</param>
    /// <param name="ok">The value for the <see cref="Ok{T,TError}"/> case.</param>
    /// <param name="error">The value for the <see cref="Error{T,TError}"/> case.</param>
    /// <returns>A <see cref="Result{T,TError}"/>.</returns>
    public static Result<T, TError> When<T, TError>(bool condition, T ok, TError error) =>
        condition ? new Ok<T, TError>(ok) : new Error<T, TError>(error);
}