﻿namespace NileDB;

// ReSharper disable UnusedMember.Global These are a utility classes. Any unused members may be so for now, but I do want these to be available for future use. Subject to future inspection, though.

/// <summary>
/// Allows replying to messages that were posted to an <see cref="Agent{TMessage}"/>.
/// </summary>
/// <typeparam name="TReply">The type of the reply.</typeparam>
internal class AsyncReplyChannel<TReply> {
    readonly TaskCompletionSource<TReply> _replyCompletionSource;

    /// <summary>
    /// Initializes a new <see cref="AsyncReplyChannel{TReply}"/>.
    /// </summary>
    public AsyncReplyChannel() {
        _replyCompletionSource = new TaskCompletionSource<TReply>();
    }

    /// <summary>
    /// Sends the given <paramref name="reply"/> to the caller that posted the message to the actor.
    /// </summary>
    /// <param name="reply">The reply to send.</param>
    public void Reply(TReply reply) => _replyCompletionSource.SetResult(reply);

    /// <summary>
    /// Gets a <see cref="Task"/> that completes once the reply is sent.
    /// </summary>
    public Task<TReply> Completion => _replyCompletionSource.Task;
}

/// <summary>
/// Allows replying to messages that were posted to an <see cref="Agent{TMessage}"/>.
/// </summary>
internal class AsyncReplyChannel {
    readonly TaskCompletionSource _replyCompletionSource;

    /// <summary>
    /// Initializes a new <see cref="AsyncReplyChannel{TReply}"/>.
    /// </summary>
    public AsyncReplyChannel() {
        _replyCompletionSource = new TaskCompletionSource();
    }

    /// <summary>
    /// Sends signal to the caller that posted the message to the actor.
    /// </summary>
    public void Reply() => _replyCompletionSource.SetResult();

    /// <summary>
    /// Gets a <see cref="Task"/> that completes once the reply is sent.
    /// </summary>
    public Task Completion => _replyCompletionSource.Task;
}