﻿namespace NileDB.Extensions; 

public static class ExtensionsForSpan {
    /// <summary>
    /// Iterates over the <paramref name="span" /> and applies a function to each element in order to produce a state.
    /// </summary>
    /// <typeparam name="T">The type of element contained by the span.</typeparam>
    /// <typeparam name="TState">The type of the produced state.</typeparam>
    /// <param name="span">The span to fold.</param>
    /// <param name="folder">The folding function.</param>
    /// <param name="state">The initial state to feed into the fold.</param>
    /// <returns>A <typeparamref name="TState" />.</returns>
    public static TState Fold<T, TState>(this ReadOnlySpan<T> span, Func<TState, T, TState> folder, TState state) {
        while (true) {
            if (span.Length == 0) return state;
            state = folder(state, span[0]);
            span = span[1..];
        }
    }
}