﻿namespace NileDB.Modules.WriteAheadLog;

using Extensions;

// Implementation courtesy of https://rosettacode.org/wiki/CRC-32#C.23

/// <summary>
/// Performs 32-bit reversed cyclic redundancy checks.
/// </summary>
public class Crc32 {
    readonly uint _value;
    static readonly uint[] ChecksumTable;

    /// <summary>
    /// Generator polynomial (modulo 2) for the reversed CRC32 algorithm. 
    /// </summary>
    const uint Generator = 0xEDB88320;
    
    /// <summary>
    /// Initializes a new <see cref="Crc32"/>.
    /// </summary>
    static Crc32() {
        ChecksumTable = Enumerable.Range(0, 256).Select(i => {
            var tableEntry = (uint)i;
            for (var j = 0; j < 8; ++j) {
                tableEntry = ((tableEntry & 1) != 0)
                    ? (Generator ^ (tableEntry >> 1))
                    : (tableEntry >> 1);
            }
            return tableEntry;
        }).ToArray();
    }

    /// <summary>
    /// Initializes a new <see cref="Crc32"/>.
    /// </summary>
    /// <param name="value">The value of the checksum.</param>
    public Crc32(uint value) {
        _value = value;
    }
    
    /// <summary>
    /// Calculates the checksum of the byte stream.
    /// </summary>
    /// <param name="input">The byte stream to calculate the checksum for.</param>
    /// <returns>A 32-bit reversed checksum.</returns>
    public static Crc32 Create(ReadOnlySpan<byte> input) {
        try {
            return new Crc32(~input.Fold((checksumRegister, currentByte) =>
                      (ChecksumTable[(checksumRegister & 0xFF) ^ currentByte] ^ (checksumRegister >> 8)), 0xFFFFFFFF));
        } catch (FormatException e) {
            throw new Exception("Could not read the stream out as bytes.", e);
        } catch (InvalidCastException e) {
            throw new Exception("Could not read the stream out as bytes.", e);
        } catch (OverflowException e) {
            throw new Exception("Could not read the stream out as bytes.", e);
        }
    }

    /// <summary>
    /// Verifies whether the calculated checksum for the given <paramref name="input"/> is equal to the checksum in the current instance.
    /// </summary>
    /// <param name="input">The byte array to verify.</param>
    /// <returns><c>true</c> if the given <paramref name="input"/> yields the same checksum as the current instance, otherwise <c>false</c>.</returns>
    public bool Verify(ReadOnlySpan<byte> input) {
        var other = Crc32.Create(input);
        return _value == other._value;
    }

    /// <summary>
    /// Casts the given <see cref="Crc32"/> to a <see cref="uint"/>.
    /// </summary>
    /// <param name="instance">The <see cref="Crc32"/> to cast.</param>
    public static implicit operator uint(Crc32 instance) => instance._value;
}