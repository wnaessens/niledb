﻿namespace NileDB.Modules.WriteAheadLog; 

/// <summary>
/// Asynchronously provides a new stream to serve as the underlying storage medium for the write ahead log.
/// </summary>
/// <returns>An awaitable <see cref="Task"/>, which resolves to a <see cref="Stream"/>.</returns>
public delegate Task<Stream> StreamFactory();

/// <summary>
/// A configuration record that governs the behaviour of a <see cref="Writer"/>.
/// </summary>
/// <param name="StreamFactory">The <see cref="StreamFactory"/> that provides the writer with a stream to write to.</param>
/// <paramref name="InitialLogSequence">The log sequence number of the first log entry.</paramref>
public record WriterConfiguration(StreamFactory StreamFactory, long InitialLogSequence);