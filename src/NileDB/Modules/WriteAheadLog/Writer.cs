﻿using System.Buffers;
using System.Buffers.Binary;

namespace NileDB.Modules.WriteAheadLog;

/// <summary>
/// Provides write access to the write ahead log by implementing a singular update queue, maintaining order while processing writes asynchronously so as to not block the caller.
/// </summary>
public class Writer : IAsyncDisposable {
    readonly Stream _stream;
    readonly Agent<(byte[], AsyncReplyChannel)> _singularUpdateQueue;

    /// <summary>
    /// Initializes a new <see cref="Writer"/>.
    /// </summary>
    /// <param name="stream">The underlying <see cref="Stream"/> where log entries are written to.</param>
    /// <param name="logSequenceNumber">The log sequence number of the first log entry.</param>
    Writer(Stream stream, long logSequenceNumber) {
        _stream = stream;
        _singularUpdateQueue = new Agent<(byte[], AsyncReplyChannel)>(message => {
            var (entry, rc) = message;

            // append the entry to the log
            Append(_stream, entry, logSequenceNumber);

            // increment the log sequence number for the next entry
            logSequenceNumber++;

            // reply to signal completion
            rc.Reply();
        });
    }

    /// <summary>
    /// Asynchronously creates a new <see cref="Writer"/>.
    /// </summary>
    /// <param name="configuration">A <see cref="WriterConfiguration"/> that governs the behaviour of the returned writer.</param>
    /// <returns>An awaitable <see cref="Task"/>, which resolves to a <see cref="Writer"/>.</returns>
    public static async Task<Writer> CreateAsync(WriterConfiguration configuration) => new(await configuration.StreamFactory(), configuration.InitialLogSequence);

    /// <summary>
    /// Asynchronously writes the given <paramref name="payload"/> to the log.
    /// </summary>
    /// <param name="payload"></param>
    /// <returns></returns>
    public Task WriteAsync(byte[] payload) =>
        _singularUpdateQueue.PostAndReply(rc => (payload, rc));

    static void Append(Stream stream, byte[] entry, long lsn) {
        // record layout:
        // LEN 4b | LSN 8b | TIMESTAMP 8b + 4b | PAYLOAD (len b) | CRC 4b
        // rent a buffer for the size of the record: sizeof header + sizeof payload
        var len = entry.Length;
        var buffer = ArrayPool<byte>.Shared.Rent(28 + len);
        
        // and envelope it with a span to minimize allocations
        var span = buffer.AsSpan();

        // DateTimeOffset provides global exact point in time
        var timestamp = DateTimeOffset.Now;

        // write LEN, LSN, TIMESTAMP and PAYLOAD first
        BinaryPrimitives.WriteInt32LittleEndian(span[..4], len);
        BinaryPrimitives.WriteInt64LittleEndian(span.Slice(4, 8), lsn);
        BinaryPrimitives.WriteInt64LittleEndian(span.Slice(12, 8), timestamp.DateTime.ToBinary());
        BinaryPrimitives.WriteInt32LittleEndian(span.Slice(20, 4), (int)timestamp.Offset.TotalMinutes);
        entry.CopyTo(span.Slice(24, len));

        // calculate CRC based on LSN, TIMESTAMP and PAYLOAD
        BinaryPrimitives.WriteUInt32LittleEndian(span.Slice(24 + len, 4), Crc32.Create(span.Slice(4, len + 20)));

        // finally, write it all to the stream
        stream.Write(span[..(28 + len)]);

        // and return the buffer to the pool
        ArrayPool<byte>.Shared.Return(buffer);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources asynchronously.
    /// </summary>
    /// <returns>A task that represents the asynchronous dispose operation.</returns>
    public async ValueTask DisposeAsync() {
        await _singularUpdateQueue.DisposeAsync();
        await _stream.FlushAsync();
        await _stream.DisposeAsync();
    }
}