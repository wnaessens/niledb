﻿namespace NileDB.Modules.WriteAheadLog; 

public class WriteAheadLogReadException : Exception {
    /// <summary>
    /// Gets the <see cref="ReadError"/> that explains why the write ahead log couldn't be read.
    /// </summary>
    public ReadError Reason { get; }

    /// <summary>
    /// Gets the ordinal number within the WAL file of the record that couldn't be read.
    /// </summary>
    public int RecordIndex { get; }

    /// <summary>
    /// Gets the position within the WAL file where the read error occurred.
    /// </summary>
    public long Position { get; }

    /// <summary>
    /// Initializes a new <see cref="WriteAheadLogReadException"/>.
    /// </summary>
    /// <param name="reason">The <see cref="ReadError"/>, telling us why the write ahead log couldn't be read.</param>
    /// <param name="recordIndex">The ordinal number within the WAL file of the record that couldn't be read.</param>
    /// <param name="position">The position within the WAL file where the read error occurred.</param>
    public WriteAheadLogReadException(ReadError reason, int recordIndex, long position) {
        Reason = reason;
        RecordIndex = recordIndex;
        Position = position;
    }
}