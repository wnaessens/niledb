﻿using System.Buffers;
using System.Buffers.Binary;

namespace NileDB.Modules.WriteAheadLog;

public enum ReadError {
    /// <summary>
    /// Error code that indicates that a record could not conform to the expected format. This probably indicates a corrupt WAL file.
    /// </summary>
    UnreadableRecord,

    /// <summary>
    /// Error code that indicates a corrupt WAL record, detectable by a failed CRC check.
    /// </summary>
    CyclicRedundancyCheckFailed
}

/// <summary>
/// Provides read access to a WAL file.
/// </summary>
public class Reader {
    readonly Stream _stream;

    /// <summary>
    /// Initializes a new <see cref="Reader"/>.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to read from.</param>
    public Reader(Stream stream) => _stream = stream ?? throw new ArgumentNullException(nameof(stream));

    /// <summary>
    /// Reads the write ahead log file and returns it as a sequence of byte arrays.
    /// </summary>
    /// <returns>A <see cref="IEnumerable{T}"/>.</returns>
    /// <exception cref="WriteAheadLogReadException">Thrown when a record in the write ahead log could not be read.</exception>
    public IEnumerable<byte[]> Read() {
        // rewind the stream before reading
        _stream.Seek(0L, SeekOrigin.Begin);
        var recordCount = 0;
        while (_stream.Position < _stream.Length) {
            yield return ReadVerifiedRecord(_stream).Match(
                ok: payload => payload,
                error => throw new WriteAheadLogReadException(error, recordCount, _stream.Position)
            );
            recordCount++;
        }
    }

    #region Read methods
    /// <summary>
    /// Attempts to read a number of bytes from the given <see cref="Stream"/>.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to read from.</param>
    /// <param name="count">The number of bytes to read.</param>
    /// <returns>A <see cref="Memory{T}"/> buffer, wrapped in an <see cref="Ok{T,TError}"/> result if the expected number of bytes was read, otherwise a <see cref="Error{T,TError}"/> result.</returns>
    static Result<Memory<byte>, ReadError> ReadBytes(Stream stream, int count) {
        using var owner = MemoryPool<byte>.Shared.Rent(count);
        var buffer = owner.Memory[..count];
        var read = stream.Read(buffer.Span);

        // result depends on whether we were able to read the expected amount of bytes
        return Result.When(read == count,
            ok: buffer,
            error: ReadError.UnreadableRecord
        );
    }

    /// <summary>
    /// Attempts to read the LEN field of a log record.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to read from.</param>
    /// <returns>A <see cref="int"/>, wrapped in a <see cref="Ok{T,TError}"/> result if the LEN field was read successfully, otherwise a <see cref="Error{T,TError}"/> result.</returns>
    static Result<int, ReadError> ReadLEN(Stream stream) =>
        // LEN field is the first 4 bytes in the record 
        ReadBytes(stream, 4)
            .Bind(buffer => BinaryPrimitives.ReadInt32LittleEndian(buffer.Span));

    /// <summary>
    /// Attempts to read a log record by first reading the LEN field and then the rest of the record base on read value of the LEN field.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to read from.</param>
    /// <returns>A <see cref="Memory{T}"/> region, wrapped in a <see cref="Ok{T,TError}"/> result if the record was read successfully, otherwise a <see cref="Error{T,TError}"/> result.</returns>
    static Result<(int len, Memory<byte> record), ReadError> ReadRecord(Stream stream) =>
        // read the LEN field
        ReadLEN(stream).Bind(len =>
            // then read the rest of the record
            ReadBytes(stream, len + 24).Bind(record => (len, record)));

    /// <summary>
    /// Attempts to read a log record and verify its CRC32 checksum.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to read from.</param>
    /// <returns>A <see><cref>byte[]</cref></see>, wrapped in a <see cref="Ok{T,TError}"/> result if the record was read and verified successfully, otherwise a <see cref="Error{T,TError}"/> result.</returns>
    static Result<byte[], ReadError> ReadVerifiedRecord(Stream stream) =>
        ReadRecord(stream).Bind(tuple => {
            var (len, record) = tuple;

            // read the checksum
            var span = record.Span;
            var crc = new Crc32(BinaryPrimitives.ReadUInt32LittleEndian(span.Slice(len + 20, 4)));

            // and use it to verify the record, the verified portion only includes the LSN, TIMESTAMP and PAYLOAD fields of the record
            return Result.When(crc.Verify(span[..(len + 20)]),
                ok: span.Slice(20, len).ToArray(),
                error: ReadError.CyclicRedundancyCheckFailed
            );
        });

    #endregion
}