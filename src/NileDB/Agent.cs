﻿using System.Threading.Tasks.Dataflow;

// ReSharper disable UnusedMember.Global This is a utility class. Any unused members may be so for now, but I do want these to be available for future use. Subject to future inspection, though.

namespace NileDB;

/// <summary>
/// Simple TPL DataFlow based actor model, heavily inspired by F# MailboxProcessor.
/// </summary>
/// <typeparam name="TMessage">The type of handled message.</typeparam>
internal class Agent<TMessage> : IAsyncDisposable {
    readonly ActionBlock<TMessage> _action;
    
    /// <summary>
    /// Initializes a new <see cref="Agent{TMessage}"/>.
    /// </summary>
    /// <param name="handler">A <see cref="Func{TResult}"/> that asynchronously handles a <typeparamref name="TMessage"/>.</param>
    public Agent(Func<TMessage, Task> handler) {
        _action = new ActionBlock<TMessage>(handler);
    }

    /// <summary>
    /// Initializes a new <see cref="Agent{TMessage}"/>.
    /// </summary>
    /// <param name="handler">A <see cref="Action{TMessage}"/> that asynchronously handles a <typeparamref name="TMessage"/>.</param>
    public Agent(Action<TMessage> handler) {
        _action = new ActionBlock<TMessage>(handler);
    }

    /// <summary>
    /// Posts a message to the actor.
    /// </summary>
    /// <param name="message">The message to post.</param>
    public void Post(TMessage message) => _action.Post(message);

    /// <summary>
    /// Post a message and asynchronously await a reply.
    /// </summary>
    /// <typeparam name="TReply">The type of the reply to receive.</typeparam>
    /// <param name="messageBuilder">A <see cref="Func{TResult}"/> that builds the message to post.</param>
    /// <returns>An awaitable <see cref="Task"/>, that resolves to a <typeparamref name="TReply"/> once the reply is received.</returns>
    public Task<TReply> PostAndReply<TReply>(Func<AsyncReplyChannel<TReply>, TMessage> messageBuilder) {
        // create a new reply channel
        var replyChannel = new AsyncReplyChannel<TReply>();

        // and pass it to the message builder func, the implementor decides how to use it when constructing the message to post.
        Post(messageBuilder(replyChannel));

        // return a task that completes once the reply is received
        return replyChannel.Completion;
    }

    /// <summary>
    /// Post a message and asynchronously await a reply.
    /// </summary>
    /// <param name="messageBuilder">A <see cref="Func{TResult}"/> that builds the message to post.</param>
    /// <returns>An awaitable <see cref="Task"/>, that resolves once the reply is received.</returns>
    public Task PostAndReply(Func<AsyncReplyChannel, TMessage> messageBuilder) {
        // create a new reply channel
        var replyChannel = new AsyncReplyChannel();

        // and pass it to the message builder func, the implementor decides how to use it when constructing the message to post.
        Post(messageBuilder(replyChannel));

        // return a task that completes once the reply is received
        return replyChannel.Completion;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources asynchronously.
    /// </summary>
    /// <returns>A task that represents the asynchronous dispose operation.</returns>
    public ValueTask DisposeAsync() {
        _action.Complete();
        return new ValueTask(_action.Completion);
    }
}